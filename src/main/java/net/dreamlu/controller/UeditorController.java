package net.dreamlu.controller;

import com.baidu.ueditor.ActionEnter;
import com.jfinal.core.Controller;
import com.jfinal.kit.PathKit;

/**
 * Ueditor编辑器
 * @author L.cm
 * email: 596392912@qq.com
 * site:http://www.dreamlu.net
 * date: 2015年9月13日 上午9:44:13
 */
public class UeditorController extends Controller {

	public void index() {}

	public void controller() {
		String rootPath = PathKit.getWebRootPath();

		// *注意*：Ueditor中查找config.json是按照 rootPath + request.getRequestURI() 的上层目录查找的
		// 这里的目录为 rootPath + "/ueditor/controller" 的上层目录
		// 即配置文件完整目录为 rootPath + "/ueditor/config.json"
		String outText = new ActionEnter( getRequest(), rootPath ).exec();

		renderHtml(outText.replace("//", "/"));
	}

}
