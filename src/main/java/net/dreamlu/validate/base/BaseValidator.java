package net.dreamlu.validate.base;

import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.jfinal.validate.Validator;

import net.dreamlu.ext.render.JCaptchaRender;

/**
 * 通用校验器
 * @author L.cm
 * email: 596392912@qq.com
 * site:http://www.dreamlu.net
 * date: 2015年7月30日 下午8:28:11
 */
public abstract class BaseValidator extends Validator {

	/**
	 * Validate JCaptcha
	 */
	protected void validateJCaptcha(String field, String errorKey, String errorMessage) {
		Controller c = getController();
		if (StrKit.isBlank(field)) {
			addError(errorKey, errorMessage);
		}
		String value = c.getPara(field);
		boolean result = JCaptchaRender.validate(c, value);
		if (!result) {
			addError(errorKey, errorMessage);
		}
	}
}
